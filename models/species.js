import {Model, Collection} from 'vue-mc'

/**
 * Task model
 */
export default class Species extends Model {

  // Default attributes that define the "empty" state.
  //taxonid":59,"kingdom_name":"ANIMALIA","phylum_name":"MOLLUSCA","class_name":"GASTROPODA","order_name":"STYLOMMATOPHORA","family_name":"VALLONIIDAE","genus_name":"Acanthinula","scientific_name":"Acanthinula spinifera","infra_rank":null,"infra_name":null,"population":null,"category":"DD
  defaults() {
    return {
      taxonid:   null,
      scientific_name: '',
    }
  }

  // Attribute mutations.
  mutations() {
    return {
      taxonid:   (id) => Number(id) || null,
      scientific_name: String,
    }
  }

  // Attribute validation
  validation() {
    return {
      taxonid:   integer.and(min(1)).or(equal(null)),
      scientific_name: string.and(required),
    }
  }

  // Route configuration
  routes() {
    return {
      fetch: '/species/{id}',
      save:  '/species',
    }
  }
}


